FROM debian:bullseye-slim AS build

RUN echo "deb-src http://deb.debian.org/debian/ bullseye main contrib non-free" > /etc/apt/sources.list.d/src.list
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install \
	build-essential \
	dpkg-dev \
	libexpat1-dev \
	automake \
	autoconf \
	libtool \
	libltdl-dev \
	bison \
	flex \
	pkg-config && \
    cd /tmp && apt-get source graphviz && \
    ln -s /tmp/graphviz-* /tmp/graphviz-src

WORKDIR /tmp/graphviz-src
RUN ./autogen.sh NOCONFIG && \
    ./configure --without-freetype2 --disable-go --disable-r --disable-swig --disable-java --disable-python2 --disable-python3 --without-glut --enable-static --disable-shared --disable-guile --disable-lua --disable-php --disable-ruby --disable-tcl --with-qt=no --disable-ltdl --with-libgd=no --with-pangocairo=no && \
    make -j4 && \
    rm cmd/dot/dot_static && \
    make -C cmd/dot dot_static LDFLAGS="-all-static" && \
    strip cmd/dot/dot_static

FROM scratch
COPY --from=build /tmp/graphviz-src/cmd/dot/dot_static /usr/bin/dot

